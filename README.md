# Performance-Analysis-Python
Python 3.7.x+ Performance Analysis.

## Getting Started

### Installation
#### Ubuntu 18.04 (Debian-based Linux)
```shell script
python3.7 -m pip install git+ssh://git@gitlab.com/<path>
```
```shell script
python3.7 -m pip install git+https://gitlab-ci-token:<personal_access_token>@gitlab.com/<path>
```
#### Windows 10
```shell script
pip install git+ssh://git@gitlab.com/<path>
```
```shell script
pip install git+https://gitlab-ci-token:<personal_access_token>@gitlab.com/<path>
```
#### Requirements.txt
For development of other projects
```
performanceanalysispy@ git+ssh://git@gitlab.com/backtester-python/performance-analysis-python
```

### Usage
#### Performance Matrices
```python
import pandas as pd
from performanceanalysispy.performance_matrices import PerformanceMatrices

d = pd.Series(0.0)  # PnL in terms of pd.Series with the datetime as index.
pm = PerformanceMatrices(d)
pm.fetch_annualized_sharpe_ratio()
pm.performance_plot()
```