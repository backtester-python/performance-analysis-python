from setuptools import setup, find_packages

version = {}
with open("performanceanalysispy/version.py") as file:
    exec(file.read(), version)

with open("README.md", "r") as fh:
    long_description = fh.read()

setup(
    name="performanceanalysispy",
    version=version['__version__'],
    author="Ernest Yuen",
    author_email="ernestyuen08@gmail.com",
    description="Performance Analysis",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/backtester-python/performance-analysis-python",
    packages=find_packages(),
    install_requires=[
        "certifi==2020.6.20",
        "cycler==0.10.0",
        "kiwisolver==1.2.0",
        "matplotlib==3.3.1",
        "numpy==1.19.1",
        "pandas==1.1.1",
        "Pillow==7.2.0",
        "pyparsing==2.4.7",
        "python-dateutil==2.8.1",
        "pytz==2020.1",
        "six==1.15.0",
    ],
    classifiers=[
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.7',
)
